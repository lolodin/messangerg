from flask import Flask, request, abort
import time
import messenger_db as mdb

app = Flask(__name__)


# Просто стриница приветствия
@app.route("/")
def hello():
    return "<h1>Добро пожаловать в Skillbox MessengerG!</h1>"


# Можно посмотреть статус MessengerG
@app.route("/status")
def status():
    messages_count = mdb.UserMessages.select().count()
    users_count = mdb.UserMessages.select(mdb.UserMessages.user_name).distinct().count()

    return {
        'status': True,
        'name': 'Skillbox MessengerG',
        'time': time.strftime("%F %T", time.localtime()),
        'users_count': users_count,
        'messages_count': messages_count,
    }


# Отправляем сообщение на сервер и добавляем в его(сообщение) в базу Skillbox messenger
@app.route("/send", methods=['POST'])
def send_message():
    data = request.json
    if not isinstance(data, dict):
        return abort(400)

    name = data.get('name')
    text = data.get('text')

    if not isinstance(name, str) or len(name) == 0:
        return abort(400)

    if not isinstance(text, str) or len(text) == 0 or len(text) > 1000:
        return abort(400)

    # Добавляем сообщение в базу Skillbox messenger
    mdb.UserMessages.create(user_name=name, user_message=text, push_time=time.time())

    return {'ok': True}


# Получаем сообщения сервером из базы Skillbox messenger
@app.route('/messages')
def get_messages():
    # Получаем последнюю дату сообщения. Для получения новых сообщений.
    try:
        after = float(request.args['after'])
    except:
        return abort(400)

    response = []

    # Читаем сообщения из базы Skillbox messenger
    messages_db = mdb.UserMessages.select().limit(50)
    messages = messages_db.dicts().execute()
    for message in messages:
        if message['push_time'] > after:
            response.append(message)

    # Ограничиваем максимальный запрос к базе данных
    return {'messages': response[:50]}


app.run()
