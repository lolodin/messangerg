from peewee import *

conn = SqliteDatabase('Skillbox_messenger.db')


class BaseModel(Model):
    class Meta:
        database = conn


class UserMessages(BaseModel):
    user_id = AutoField(column_name='UserId', verbose_name='User Id')
    user_name = TextField(column_name='Name', verbose_name='User name')
    user_message = TextField(column_name='Message', verbose_name='User message')
    push_time = IntegerField(column_name='Time', verbose_name='Push time')

    class Meta:
        table_name = 'UserMessages'


UserMessages.create_table()